$(function() {
  $('[data-check="all"]').change(function () {
    $('[data-check="check"]').prop('checked', $(this).prop("checked")).closest('.assets-table__row').toggleClass('assets-table__row--checked', this.checked);;
  });
  $('[data-check="check"]').change(function() {
    $(this).closest('.assets-table__row').toggleClass('assets-table__row--checked', this.checked);
  });
});



function tabs(tabActiveClass, contentActiveClass) {
  $('[data-tab="menu"] a').on("click", function (e) {
    var dataTab = $(this).data("tab");
    $(this).parents('[data-tab="tabs"]').removeClass('results-box--active');
    $(this).siblings().removeClass(tabActiveClass);
    $(this).addClass(tabActiveClass);
    $(this).parents('[data-tab="tabs"]').find('[data-content="' + dataTab + '"]').addClass(contentActiveClass).siblings().removeClass(contentActiveClass);
    e.preventDefault();
  });
}
tabs("results-box__tab--active", "results-box__content--active");

function tabsToDropdown(tabsActiveClass) {
  $('[data-tab="menu"] a').on('click', function (tabsActiveClass) {
    if (window.matchMedia("(max-width: 767px)").matches) {
      var selText = $(this).text();
      $(this).parents('[data-tab="tabs"]').find('[data-tab="toggle-btn"]').html(selText);
      $(this).parents('[data-tab="tabs"]').removeClass(tabsActiveClass);
    }
  });
  $('[data-tab="toggle-btn"]').on('click', function () {
    $(this).parents('[data-tab="tabs"]').toggleClass(tabsActiveClass);
  })
}

tabsToDropdown('results-box--active');

